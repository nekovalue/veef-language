import { argv } from "process";
import Lexer from "./Lexer";
import Parser from "./Parser";

const fs = require('fs');
const path = require('path');

const code = fs.readFileSync(path.resolve(__dirname, '../veef_files/', argv[2]), {encoding: 'UTF-8'});


const lexer = new Lexer(code);

lexer.lexAnalysis();

const parser = new Parser(lexer.tokenList);

const rootNode = parser.parseCode();

parser.run(rootNode);